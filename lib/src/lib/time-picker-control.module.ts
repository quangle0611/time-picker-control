import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { TimePickerComponent } from './time-picker-control.component';



@NgModule({
  imports: [
    CommonModule,
    NgbTimepickerModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    TimePickerComponent
  ],
  declarations: [
    TimePickerComponent
  ],
  providers: [],
})
export class TimePickerModule { }
