import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs';
import { ValueAccessorBase } from './control-value-accessor';


@Component({
  selector: 'time-picker-control',
  templateUrl: 'time-picker-control.component.html',
  styleUrls: ['./time-picker-control.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: TimePickerComponent,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: TimePickerComponent,
      multi: true,
    },
  ],
})
export class TimePickerComponent extends ValueAccessorBase<string> implements OnInit {

  @Input() inputValue: any;
  formControl: FormControl;

  @Output() bindToChange = new EventEmitter<string>();
  @Output() bindToBlur = new EventEmitter<string>();

  onEditorChange$ = new Subject();

  constructor() {
    super();
  }

  ngOnInit() {
    console.log('inputValue', this.inputValue)
    console.log('value', this.innerValue)
    // this.Editor = this.Editor.default ? this.Editor.default : this.Editor;
    // this.onEditorChange$
    //   .pipe(debounce(() => timer(500))) // Update every 0.5s to reduce the lagging
    //   .subscribe((editor: any) => {
    //     const data = editor.getData();
    //     this.value = data;
    //     this.bindToChange.emit(this.value);
    //   });
  }
}
