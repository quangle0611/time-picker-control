import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { TimePickerControlComponent } from './time-picker-control.component';



@NgModule({
  declarations: [
    TimePickerControlComponent
  ],
  imports: [
    CommonModule,
    NgbTimepickerModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    TimePickerControlComponent
  ]
})
export class TimePickerControlModule { }
