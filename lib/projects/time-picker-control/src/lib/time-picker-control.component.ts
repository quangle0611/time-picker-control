import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs';
import { ValueAccessorBase } from './control-value-accessor';


@Component({
  selector: 'lib-time-picker-control',
  templateUrl: 'time-picker-control.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: TimePickerControlComponent,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: TimePickerControlComponent,
      multi: true,
    },
  ],
})
export class TimePickerControlComponent extends ValueAccessorBase<string> implements OnInit {

  @Input() inputValue: any;
  formControl: FormControl;

  @Output() bindToChange = new EventEmitter<string>();
  @Output() bindToBlur = new EventEmitter<string>();

  onEditorChange$ = new Subject();

  constructor() {
    super();
  }

  ngOnInit() {
    console.log('inputValue', this.inputValue)
    console.log('value', this.innerValue)
    // this.Editor = this.Editor.default ? this.Editor.default : this.Editor;
    // this.onEditorChange$
    //   .pipe(debounce(() => timer(500))) // Update every 0.5s to reduce the lagging
    //   .subscribe((editor: any) => {
    //     const data = editor.getData();
    //     this.value = data;
    //     this.bindToChange.emit(this.value);
    //   });
  }
}
