import { TestBed } from '@angular/core/testing';

import { TimePickerControlService } from './time-picker-control.service';

describe('TimePickerControlService', () => {
  let service: TimePickerControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TimePickerControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
