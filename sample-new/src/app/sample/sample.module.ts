import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SampleComponent } from './sample.component';
import { TimePickerControlModule } from 'time-picker-control';

const routes: Routes = [
    {
        path: '',
        component: SampleComponent,
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        TimePickerControlModule
    ],
    declarations: [
        SampleComponent
    ],
})
export class SampleModule { }
